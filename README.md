# Deep Q-learning interface for MS-DOS games
This repository contains a deep q-learning interface for MS-DOS games. It comes with a sample Double Deep Q-Learning agent. As a sample game "The Games: Winter Challenge" (1991) is used.

## Requirements
- Ubuntu (tested on Ubuntu 16)
- Dosbox (`sudo apt install dosbox`)
- Python (tested with Python 2.7)
- Python packages (`sudo pip install -r requirements.txt`)
- Virtual display backend (`sudo apt install xvfb` when not visible and `sudo apt install xserver-xephyr` when visible)
- Xdotool for keyboard interactions (`sudo apt install xdotool`)
- An MS-DOS game

## How to run?
Before running `main.py` make sure the settings are valid. This can be done in `settings.py`. If you want to run it for "The Games: Winter Challenge", then download it from [this](http://www.bestoldgames.net/eng/old-games/winter-challenge.php) page.

## 'Out-of-the-box' supported Games 
- The Games: Winter Challenge (1991)
- Dangerous Dave (1998)