class Settings:
    # Project path
    project_path = '/mnt/3E6227E362279F21/universiteit/natural computing/winter/'

    # Dosbox path
    dosbox_path = '/home/mrplank/.dosbox/'

    # Path to game
    path = '/mnt/3E6227E362279F21/dos_games/winter/'
    # path = '/mnt/3E6227E362279F21/dos_games/dangerous_dave/'

    # Executable of game
    executable = 'WINTER.EXE'
    # executable = 'DAVE.EXE'

    info_fields = ['speed', 'time']

    # Game id (0=luge, 3=team bobsled)
    game_id = 3

    # Number of parallel threads
    # threads = 2

    # Number of runs
    runs = 3

    # Number of episodes
    episodes = 50000

    # Timeout of a single episode
    timeout = 18000

    # How many of the last frames to punish in a crash?
    punish_reward_crash = 5

    # How many frames are used per training iteration?
    batch_size = 1024    # default: 32 or 64

    # Epsilon
    epsilon = 1.0   # 1.0, dan decay 0.99816

    # Epsilon min
    epsilon_min = 0.1

    # Epsilon decay
    # Found by: solving [EPSILON] * (x^[EPISODES])=[EPSILON_MIN] for x > 0
    epsilon_decay = 0.9995   # default: 0.995

    # Learning rate
    learning_rate = 0.001 # default: 0.001

    # Discount factor
    gamma = 0.99    # default: 0.95

    # queue size
    queue_size = 2000   # default: 2000

    # Size of the frame in Height x Width x Channels
    state_size = (160, 208, 3)

    # Possible actions in xdo key format
    actions = ['', 'Left', 'Right', 'Down']
    # actions = ['', 'Left', 'Right', 'Up', 'Left+Up', 'Right+Up', 'Ctrl']

    # Default action
    default_action = 0

    # Number of actions
    action_size = len(actions)

    # Virtual Display Visible
    virtual_display_visible = 1