import time
from keras.preprocessing.image import img_to_array

from dosbox import dosbox_screenshot
from settings import Settings
from common import *


def winter_preprocess_image(rgb_image, i = -1):
    #gray_image = rgb_image.convert('L')
    frame = rgb_image.crop((112, 1, 112 + Settings.state_size[1], 1 + Settings.state_size[0]))
    frame = frame.resize((110, 84))
    frame = frame.crop((13, 0, 97, 84))
    if i > -1:
        frame.save('test_frame_%d.png' % i)
    frame_array = img_to_array(frame)
    return frame_array.reshape((1,) + frame_array.shape)


def winter_setup(display):
    # Skip intro
    press_key(display, 'Return')
    time.sleep(2.0)
    press_key(display, 'Return')
    time.sleep(2.0)

    # Setup menu
    press_key(display, 'Right')
    time.sleep(0.5)
    press_key(display, 'Right')
    time.sleep(0.5)

    press_key(display, 'Return')
    time.sleep(0.5)
    press_key(display, 'Return')
    time.sleep(1.5)

    # Training menu
    press_key(display, 'Return')
    time.sleep(1.5)

    if Settings.game_id > 0:
        for _ in xrange(Settings.game_id):
            press_key(display, 'Right')
            time.sleep(0.5)

    # Training menu
    press_key(display, 'Return')
    time.sleep(2.5)

    press_key(display, 'Return')
    time.sleep(2.5)


def winter_reset(display):
    # Make sure that in menu the right unit is selected
    im_rgb = dosbox_screenshot(display)

    while im_rgb.getpixel((200, 50)) != (85, 85, 255):
        press_key(display, 'Down')
        im_rgb = dosbox_screenshot(display)

    # Remove menu
    press_key(display, 'Return')
    time.sleep(2.5)

    # Make sure is started
    game_speed = 0.0
    if Settings.game_id == 0:
        start_key = 'Up'
    else:
        start_key = 'Return'

    while game_speed == 0.0:
        press_key_down(display, start_key)
        time.sleep(0.5)
        press_key_up(display, start_key)

        check_img = dosbox_screenshot(display)
        _, _, game_speed = extract_info(check_img)


def winter_step(display, key='', key_previous=None):
    if key_previous is not None and key_previous != key and key_previous != '':
        press_key_up(display, key_previous)
    if key != '':
        press_key_down(display, key)
    press_key_down(display, "Alt+F12", delay=1)
    # time.sleep(0.1)
    press_key_up(display, "Alt+F12", delay=1)
    #time.sleep(0.5)

    return dosbox_screenshot(display)


def extract_time_and_speed(img):
    # The pattern matching
    width = 6
    height = 8

    color_corners = (121, 121, 121)
    color_sides = (16, 16, 16)

    corners = {
        # time
        'min_1': (263, 175),
        'sec_1': (274, 175),
        'sec_2': (282, 175),
        'msec_1': (293, 175),
        'msec_2': (301, 175),

        # speed
        'km_1': (263, 185),
        'km_2': (271, 185),
        'hm_1': (282, 185),
        'hm_2': (290, 185),
    }

    result = {}

    # sides_0 = ['top', 'bottom', 'left_top', 'left_bottom', 'right_top', 'right_bottom']
    sides_1 = ['right_top', 'right_bottom']
    sides_2 = ['top', 'middle', 'bottom', 'left_bottom', 'right_top']
    sides_3 = ['top', 'middle', 'bottom', 'right_top', 'right_bottom']
    sides_4 = ['middle', 'left_top', 'right_top', 'right_bottom']
    sides_5 = ['top', 'middle', 'bottom', 'left_top', 'right_bottom']
    sides_6 = ['top', 'middle', 'bottom', 'left_top', 'left_bottom', 'right_bottom']
    sides_7 = ['top', 'right_top', 'right_bottom']
    sides_8 = ['top', 'middle', 'bottom', 'left_top', 'left_bottom', 'right_top', 'right_bottom']
    sides_9 = ['top', 'middle', 'left_top', 'right_top', 'right_bottom']

    for value, coordinates in corners.iteritems():
        color1 = img.getpixel((coordinates[0] + width, coordinates[1]))
        color2 = img.getpixel((coordinates[0], coordinates[1]))

        assert color1 == color_corners or color2 == color_corners or value == 'km_1', "Corners for time not found, %s" % (
            value)

        x, y = coordinates

        top = (x + (width / 2), y)
        middle = (x + (width / 2), y + (height / 2))
        bottom = (x + (width / 2), y + height)
        left_top = (x, y + (height / 4))
        left_bottom = (x, y + (height / 4 * 3))
        right_top = (x + width, y + (height / 4))
        right_bottom = (x + width, y + (height / 4 * 3))

        present = []
        if img.getpixel(top) == color_sides:
            present.append('top')
        if img.getpixel(middle) == color_sides:
            present.append('middle')
        if img.getpixel(bottom) == color_sides:
            present.append('bottom')
        if img.getpixel(left_top) == color_sides:
            present.append('left_top')
        if img.getpixel(left_bottom) == color_sides:
            present.append('left_bottom')
        if img.getpixel(right_top) == color_sides:
            present.append('right_top')
        if img.getpixel(right_bottom) == color_sides:
            present.append('right_bottom')

        if present == sides_1:
            result[value] = 1.0
        elif present == sides_2:
            result[value] = 2.0
        elif present == sides_3:
            result[value] = 3.0
        elif present == sides_4:
            result[value] = 4.0
        elif present == sides_5:
            result[value] = 5.0
        elif present == sides_6:
            result[value] = 6.0
        elif present == sides_7:
            result[value] = 7.0
        elif present == sides_8:
            result[value] = 8.0
        elif present == sides_9:
            result[value] = 9.0
        else:
            result[value] = 0.0

    # Warned you...
    time = result['min_1'] * 60 * 100 + result['sec_1'] * 10 * 100 + result['sec_2'] * 100 + result['msec_1'] * 10 + result['msec_2']
    if time == 0.0:
        time = 1.0
    time /= 100.0

    speed = result['km_1'] * 10 * 100 + result['km_2'] * 100 + result['hm_1'] * 10 + result['hm_2']
    speed /= 100.0
    return time, speed


def extract_done(rgb_img):
    # Check menu pixel
    menu_pixel = (76, 31)
    r, g, b = rgb_img.getpixel(menu_pixel)
    menu = (r == 251 and g == 40 and b == 40)

    # Check finish pixel GAME 3
    # finish_pixel = (21, 138)
    # r, g, b = rgb_img.getpixel(finish_pixel)
    # finish = (r != 56 or g != 186 or b != 186)

    finish_row = -1
    finish = False

    if menu:
        x = 128
        for row in range(1, 10 + 1):
            y = 57 + (row - 1) * 8
            r, g, b = rgb_img.getpixel((x, y))
            if r == 255 and g == 255 and b == 255:
                finish_row = row
                break

        x = 173
        y = 57 + (finish_row - 1) * 8

        r, g, b = rgb_img.getpixel((x, y))
        if r != 255 or g != 255 or b != 255:
            finish = True

    if menu and finish:
        done = 1
    elif menu and not finish:
        done = -1
    else:
        done = 0
    return done


def extract_info(rgb_img):
    done = extract_done(rgb_img)

    time, speed = extract_time_and_speed(rgb_img)
    return done, time, speed


def calculate_distance(distance, time, speed, next_time, next_speed):
    return distance + ((next_time - time) * ((next_speed + speed) / (2.0 * 3.6)))