import random
from collections import deque

import numpy as np
from keras import backend as K
from keras.layers import Dense, Conv2D, Flatten, Input, concatenate
from keras.models import Model
from keras.optimizers import Adam
from PIL import Image

from common import mean
from settings import Settings
from winter import winter_preprocess_image


# Adapted from: https://github.com/keon/deep-q-learning/blob/master/ddqn.py
# Useful: https://github.com/jaara/AI-blog/blob/master/Seaquest-DDQN-PER.py

class DQNAgent:
    def __init__(self, action_size):
        self.action_size = action_size
        self.memory = deque(maxlen=Settings.queue_size)
        self.gamma = Settings.gamma
        self.epsilon = Settings.epsilon
        self.epsilon_min = Settings.epsilon_min
        self.epsilon_decay = Settings.epsilon_decay
        self.learning_rate = Settings.learning_rate
        self.model = self._build_model()
        self.target_model = self._build_model()
        self.update_target_model()
        self.warm_up()

    def warm_up(self):
        current_img = Image.open('default.png')
        current_img = current_img.convert('RGB')
        current_frame = winter_preprocess_image(current_img)

        state = [current_frame, np.array([0.0]), 0.0, 0]

        self.act(state[0])

    # def huber_loss(self, y_true, y_pred):
    #     HUBER_LOSS_DELTA = 2.0
    #
    #     err = y_true - y_pred
    #
    #     cond = K.abs(err) < HUBER_LOSS_DELTA
    #     L2 = 0.5 * K.square(err)
    #     L1 = HUBER_LOSS_DELTA * (K.abs(err) - 0.5 * HUBER_LOSS_DELTA)
    #
    #     loss = tf.where(cond, L2, L1)  # Keras does not cover where function in tensorflow :-(
    #
    #     return K.mean(loss)

    def _huber_loss(self, target, prediction):
        # sqrt(1+error^2)-1
        error = prediction - target
        return K.mean(K.sqrt(1 + K.square(error)) - 1, axis=-1)

    def _build_old_model(self, input_shape=(84, 84, 3)):
        frame_input = Input(shape=input_shape, name='frame_input')

        x = Conv2D(16, kernel_size=(8, 8), strides=(2, 2),
                   activation='relu')(frame_input)
        x = Conv2D(32, kernel_size=(4, 4), strides=(2, 2),
                   activation='relu')(x)

        x = Flatten()(x)
        x = Dense(256, activation='relu')(x)
        output = Dense(self.action_size, activation='softmax')(x)

        model = Model(inputs=[frame_input], outputs=[output])
        model.compile(loss=self._huber_loss, optimizer=Adam(lr=self.learning_rate))
        return model

    def _build_model(self, input_shape=(84, 84, 3)):
        frame_input = Input(shape=input_shape, name='frame_input')

        x = Conv2D(32, kernel_size=(8, 8), strides=(2, 2),
                   activation='relu')(frame_input)
        x = Conv2D(64, kernel_size=(4, 4), strides=(2, 2),
                   activation='relu')(x)
        x = Conv2D(64, kernel_size=(3, 3), strides=(1, 1),
                   activation='relu')(x)

        x = Flatten()(x)
        x = Dense(256, activation='relu')(x)
        output = Dense(self.action_size, activation='softmax')(x)

        model = Model(inputs=[frame_input], outputs=[output])
        model.compile(loss=self._huber_loss, optimizer=Adam(lr=self.learning_rate))
        return model

    def update_target_model(self):
        # copy weights from model to target_model
        self.target_model.set_weights(self.model.get_weights())

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, min(len(self.memory), batch_size))
        losses = []
        for state, action, reward, next_state, done in minibatch:
            target = self.model.predict(state)
            if done:
                target[0][action] = reward
            else:
                a = self.model.predict(next_state)[0]
                t = self.target_model.predict(next_state)[0]
                target[0][action] = reward + self.gamma * t[np.argmax(a)]
            history = self.model.fit(state, target, epochs=1, verbose=0)
            losses.append(history.history['loss'])
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
        return mean(losses)

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)