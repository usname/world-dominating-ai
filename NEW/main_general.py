import time
import logging
from collections import Counter

from pyvirtualdisplay.smartdisplay import SmartDisplay
import multiprocessing.pool
import pandas as pd

from NEW.games.dave import Dave
from dosbox import *
from winter import *
from settings import Settings
from agent_ddqn import DQNAgent
from rewards import *


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("topdown")
logger.setLevel(logging.INFO)


def run(game):
    vdisplay = SmartDisplay(visible=Settings.virtual_display_visible, size=(640, 400))
    vdisplay.start()

    display = vdisplay.display

    logger.info("Display: %d" % vdisplay.display)

    dosbox_start(display)

    time.sleep(1.0)

    dosbox_game_start(display, Settings.path, Settings.executable)

    time.sleep(1.0)

    game.setup(display)

    time.sleep(3.0)

    game.reset(display)

    # vdisplay.stop()


def main():
    game = Dave
    run(game)


if __name__ == "__main__":
    Settings.path = '/mnt/3E6227E362279F21/dos_games/dangerous_dave/'
    Settings.executable = 'DAVE.EXE'
    Settings.actions = ['', 'Left', 'Right', 'Up', 'Left+Up', 'Right+Up', 'Ctrl']
    Settings.virtual_display_visible = 1

    main()
