import glob
import shutil
import time

from PIL import Image

from common import *
from settings import Settings


def dosbox_start(display):
    conf_file_location = os.path.join(Settings.dosbox_path, '%d.conf' % display)
    shutil.copy(os.path.join(Settings.dosbox_path, "dosbox-0.74.conf"), conf_file_location)

    with open(conf_file_location, 'r') as f:
        content = f.read()

    content = content.replace('captures=capture', 'captures=capture_%d' % display)

    with open(conf_file_location, 'w') as f:
        f.write(content)

    command(display, "dosbox -conf %s >/dev/null 2>&1 &" % conf_file_location)


def dosbox_game_start(display, path, executable):
    type_line(display, 'mount C: %s' % path)
    type_line(display, 'C:')
    type_line(display, '%s' % executable)


def dosbox_read_image(file_name):
    try:
        screenshot_img = Image.open(file_name)
        return screenshot_img.convert('RGB')
    except:
        return dosbox_read_image(file_name)


def dosbox_screenshot(display):
    press_key(display, "Ctrl+F5")

    files = glob.glob(os.path.join(Settings.dosbox_path, 'capture_%d' % display, '*.png'))
    while len(files) == 0:
        time.sleep(0.1)
        files = glob.glob(os.path.join(Settings.dosbox_path, 'capture_%d' % display, '*.png'))

    file_name = max(files, key=os.path.getctime)

    return dosbox_read_image(file_name)


def dosbox_delete_images(display):
    img_files = glob.iglob(os.path.join(Settings.dosbox_path, 'capture_%d' % display, '*.png'))
    for img_file in img_files:
        os.remove(img_file)


def dosbox_stop(display):
    press_key(display, "Ctrl+F9")
