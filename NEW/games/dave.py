from PIL import Image
from keras.preprocessing.image import img_to_array

from NEW.games.game import Game
from NEW.settings import Settings


class Dave(Game):
    @staticmethod
    def setup(display):
        pass

    @staticmethod
    def preprocess_image(rgb_image):
        frame = rgb_image.crop((112, 1, 112 + Settings.state_size[1], 1 + Settings.state_size[0]))
        frame = frame.resize((110, 84))
        frame = frame.crop((13, 0, 97, 84))
        frame.save('test_frame.png')

        # Remove top 16 pixels
        # Remove bottom 34 pixels
        # Result = 320 width x 150 height
        # Original = 320 x 200

        #frame_array = img_to_array(frame)
        #return frame_array.reshape((1,) + frame_array.shape)

    @staticmethod
    def extract_info(rgb_img):
        info = {'score': 0, 'level': 0, 'lives': 0, 'cup': False}

        life_coordinates = [(260, 6)]
        for life_coordinate in life_coordinates:
            r, g, b = rgb_img.getpixel(life_coordinate)
            if r != 0 or g != 0 or b != 0:
                info['lives'] += 1

        return info

    @staticmethod
    def reset(display):
        pass