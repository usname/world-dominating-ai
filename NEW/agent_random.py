import random


class RandomAgent:
    def __init__(self, action_size):
        self.action_size = action_size

    def remember(self, state, action, reward, next_state, done):
        pass

    def act(self):
        return random.randrange(self.action_size)

    def replay(self, batch_size):
        pass

    def load(self, name):
        pass

    def save(self, name):
        pass