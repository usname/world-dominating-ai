import time
import logging
from collections import Counter

from pyvirtualdisplay.smartdisplay import SmartDisplay
import multiprocessing.pool
import pandas as pd

from dosbox import *
from winter import *
from settings import Settings
from agent_ddqn import DQNAgent
from rewards import *


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("topdown")
logger.setLevel(logging.INFO)


# Winter functions
def winter_play(display, agent, reward_function):
    game_states = []
    previous_action = Settings.default_action
    action = Settings.default_action

    current_img = dosbox_read_image(os.path.join(Settings.project_path, 'default.png'))
    current_frame = winter_preprocess_image(current_img)

    done, game_time, speed = extract_info(current_img)
    distance = 0
    speeds_list = [0.0]
    state = [current_frame, np.array([speed]), game_time, distance, speeds_list]

    action_counter = Counter(Settings.actions)
    # i = 0
    while state[2] < Settings.timeout:

        if done != 0:
            agent.update_target_model()

            game_states[-1]['done'] = done
            game_states[-1]['next_state'] = None

            logger.info("done: %d, time: %.2f (s), speed: %.2f (km/h), distance: %d (m), avg speed: %.2f (km/h)" % (done, game_states[-1]['state'][2], game_states[-1]['state'][1], game_states[-1]['state'][3], mean(game_states[-1]['state'][4])))
            break

        action = agent.act(state[0])

        # logger.info("Key '%s'" % str(Settings.actions[action]))
        action_counter[Settings.actions[action]] += 1
        next_img = winter_step(display, Settings.actions[action], Settings.actions[previous_action])
        next_frame = winter_preprocess_image(next_img)
        # i += 1

        next_done, next_time, next_speed = extract_info(next_img)
        next_distance = calculate_distance(state[3], state[2], state[1], next_time, next_speed)

        speeds_list.append(next_speed)
        next_state = [next_frame, np.array([next_speed]), next_time, next_distance, speeds_list]

        # If the time is the same, then the unit is crashed or finished - we shouldn't remember that ;)
        if next_state[2] != state[2] and next_done == 0:
            game_state = {'state': state,
                          'action': action,
                          'reward': None,
                          'next_state': next_state,
                          'done': done}
            game_states.append(game_state)

            logger.info("done: %d, time: %.2f (s), speed: %.2f (km/h), distance: %d (m), avg speed: %.2f (km/h)" % (done, state[2], state[1], state[3], mean(state[4])))

        previous_action = action
        state = next_state
        done = next_done

    # Timeout
    if done == 0:
        done = -2
        agent.update_target_model()
        game_state = {'state': state,
                      'action': action,
                      'reward': None,
                      'next_state': None,
                      'done': done}
        game_states.append(game_state)

        press_key(display, "Escape")

        logger.info("done: -2, =timeout")

    # Calculate rewards
    game_states = reward_wrapper(game_states, getattr(Rewards, reward_function))
    reward_list = [x['reward'] for x in game_states]
    for game_state in game_states:
        if game_state['next_state'] is not None:
            next_state = game_state['next_state'][0]
        else:
            next_state = None
        agent.remember(game_state['state'][0],
                       game_state['action'],
                       game_state['reward'],
                       next_state,
                       game_state['done'])

    dosbox_delete_images(display)

    return game_states[-1], action_counter, reward_list


def run(run_id, agent, reward_function):
    vdisplay = SmartDisplay(visible=Settings.virtual_display_visible, size=(640, 400))
    vdisplay.start()

    display = vdisplay.display

    logger.info("Display: %d" % vdisplay.display)

    dosbox_start(display)

    time.sleep(1.0)

    dosbox_game_start(display, Settings.path, Settings.executable)

    time.sleep(1.0)

    winter_setup(display)

    time.sleep(3.0)

    columns = ['episode_id', 'done', 'time', 'distance', 'avg_speed', 'avg_reward', 'avg_replay_fit_loss']
    columns += map(format_action, Settings.actions)
    df = pd.DataFrame(columns=columns)
    df['episode_id'] = df['episode_id'].astype(int)
    for key in Settings.actions:
        df[format_action(key)] = df[format_action(key)].astype(int)
    df.to_csv(os.path.join(Settings.project_path,"%s_run_%d.csv" % (str(reward_function), run_id)), header=True, index=False)

    winter_reset(display)

    for episode_id in xrange(1, Settings.episodes + 1):
        logger.info("Episode %d of %d" % (episode_id, Settings.episodes))

        result, action_counter, reward_list = winter_play(display, agent, reward_function)

        avg_replay_fit_loss = agent.replay(Settings.batch_size)
        agent.save(os.path.join(Settings.project_path, 'models/%s_run_%d_episode_%d.h5' % (str(reward_function), run_id, episode_id)))

        row = {'episode_id': int(episode_id),
               'done': result['done'],
               'time': result['state'][2],
               'distance': result['state'][3],
               'avg_speed': mean(result['state'][4]),
               'avg_reward': mean(reward_list),
               'avg_replay_fit_loss': avg_replay_fit_loss}
        for key in action_counter:
            row[format_action(key)] = action_counter[key]

        df = df.append(pd.DataFrame(row, columns=columns))
        df.to_csv(os.path.join(Settings.project_path, "%s_run_%d.csv" % (str(reward_function), run_id)), header=True, index=False)

        if episode_id <= Settings.episodes:
            winter_reset(display)

    vdisplay.stop()


def main():
    reward_functions = ['reward_speed']#, 'reward_distance']
    for run_id in [4]:
        for reward_function in reward_functions:
            fix_random_seeds(run_id)

            agent = DQNAgent(Settings.action_size)

            run(run_id, agent, reward_function)
            # pool = multiprocessing.pool.ThreadPool(processes=Settings.threads)
            # for display_index in range(1, 2):
            #     pool.apply_async(run, [display_index, agent])
            # pool.close()
            # pool.join()


if __name__ == "__main__":
    main()
