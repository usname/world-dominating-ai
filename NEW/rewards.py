from settings import Settings
from common import mean
import math

#
# def reward_distance_avg_speed(done, time, distance, avg_speed):
#     return distance * avg_speed
#
# def reward_distance_per_second(done, time, distance, avg_speed):
#     # Track length = 1420 m
#     # Set time = 100 s
#     # punishment = 1420 / set time
#
#     return distance - (14.20 * time)


class Rewards:
    # Reward with goal 'finish', 0 - 1 for distance, -1 for last N frames of crash
    @staticmethod
    def reward_distance(done, time, distance, speed, avg_speed):
        if done < 0:
            return -1.0

        return 1.0
        #return distance**2 / 1420**2

    # Reward with goal 'top speed',
    @staticmethod
    def reward_speed(done, time, distance, speed, avg_speed):
        if done < 0:
            return -1.0
        if speed < 1.0:
            return -0.5

        return math.log(speed, 10) / 2.0


def reward_wrapper(game_states, reward_function):
    updated_game_states = []
    game_state_final = game_states[-1]

    for key, game_state in enumerate(game_states):
        done = game_state['done']
        if game_state_final['done'] == -2:
            done = -2
        if game_state_final['done'] == -1 and key >= (len(game_states) - Settings.punish_reward_crash):
            done = -1
        game_state['reward'] = float(reward_function(done, game_state['state'][2], game_state['state'][3], float(game_state['state'][1]), mean(game_state['state'][4])))
        updated_game_states.append(game_state)

    return updated_game_states
