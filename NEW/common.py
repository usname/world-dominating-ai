import os
import random
import numpy as np


def command(display, command):
    os.system("export DISPLAY=:" + str(display) + ";" + command)


def type_line(display, text):
    command(display, "xdotool type '" + text + "'; xdotool key Return")


def press_key(display, key):
    command(display, "xdotool key %s" % key)


def press_key_up(display, key, delay=12):
    if delay != 12:
        command(display, "xdotool keyup --delay %d %s" % (delay, key))
    else:
        command(display, "xdotool keyup %s" % key)


def press_key_down(display, key, delay=12):
    if delay != 12:
        command(display, "xdotool keydown --delay %d %s" % (delay, key))
    else:
        command(display, "xdotool keydown %s" % key)


def debug_screenshot(display, file_name=None):
    if file_name is None:
        file_name = "screenshot_" + str(display) + ".png"
    os.system("DISPLAY=:" + str(display) + " import -window root %s" % file_name)


def fix_random_seeds(run_id):
    random.seed(2000 + run_id)
    np.random.seed(2000 + run_id)


def mean(l):
    return np.array(l).mean()


def format_action(action):
    if action == '':
        return 'action_none'
    else:
        return 'action_' + action.lower()
