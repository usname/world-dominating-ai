from glob import glob

from natsort import natsorted
from keras.preprocessing.image import img_to_array
from keras.layers import Dense, Conv2D, Flatten, Input, concatenate
from keras.models import Model
import numpy.ma as ma
import pylab as pl
import matplotlib.cm as cm
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable

from PIL import Image

from settings import Settings


# From : https://github.com/philipperemy/keras-visualize-activations
def get_activations(model, model_inputs, print_shape_only=False, layer_name=None):
    import keras.backend as K
    print('----- activations -----')
    activations = []
    inp = model.input

    model_multi_inputs_cond = True
    if not isinstance(inp, list):
        # only one input! let's wrap it in a list.
        inp = [inp]
        model_multi_inputs_cond = False

    outputs = [layer.output for layer in model.layers if
               layer.name == layer_name or layer_name is None]  # all layer outputs

    funcs = [K.function(inp + [K.learning_phase()], [out]) for out in outputs]  # evaluation functions

    if model_multi_inputs_cond:
        list_inputs = []
        list_inputs.extend(model_inputs)
        list_inputs.append(1.)
    else:
        list_inputs = [model_inputs, 1.]

    # Learning phase. 1 = Test mode (no dropout or batch normalization)
    # layer_outputs = [func([model_inputs, 1.])[0] for func in funcs]
    layer_outputs = [func(list_inputs)[0] for func in funcs]
    for layer_activations in layer_outputs:
        activations.append(layer_activations)
        if print_shape_only:
            print(layer_activations.shape)
        else:
            print(layer_activations)
    return activations


def load_data():
    def winter_preprocess_image(rgb_image):
        # gray_image = rgb_image.convert('L')
        frame = rgb_image.crop((112, 1, 112 + Settings.state_size[1], 1 + Settings.state_size[0]))
        frame = frame.resize((110, 84))
        frame = frame.crop((13, 0, 97, 84))
        frame_array = img_to_array(frame)
        return frame_array

    X = []

    # Visualize the first layer of convolutions on an input image
    file_names = glob('../test_frame_*.png')
    for file_name in file_names:
        img = Image.open(file_name)
        img = img.convert('RGB')
        X.append(winter_preprocess_image(img))

    return X


def get_model():
    input_shape = (84, 84, 3)
    frame_input = Input(shape=input_shape, name='frame_input')

    convout1 = Conv2D(32, kernel_size=(8, 8), strides=(2, 2),
                      activation='relu', name='conv1')(frame_input)
    convout2 = Conv2D(64, kernel_size=(4, 4), strides=(2, 2),
                      activation='relu', name='conv2')(convout1)
    convout3 = Conv2D(64, kernel_size=(3, 3), strides=(1, 1),
                      activation='relu')(convout2)

    x = Flatten()(convout3)
    x = Dense(256, activation='relu')(x)
    output = Dense(4, activation='softmax')(x)

    model = Model(inputs=[frame_input], outputs=[output])
    return model


def nice_imshow(ax, data, vmin=None, vmax=None, cmap=None):
    """Wrapper around pl.imshow"""
    if cmap is None:
        cmap = cm.jet
    if vmin is None:
        vmin = data.min()
    if vmax is None:
        vmax = data.max()
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    im = ax.imshow(data, vmin=vmin, vmax=vmax, interpolation='nearest', cmap=cmap)
    pl.colorbar(im, cax=cax)
    pl.show()


def make_mosaic(imgs, nrows, ncols, border=1):
    """
    Given a set of images with all the same shape, makes a
    mosaic with nrows and ncols
    """
    nimgs = imgs.shape[0]
    imshape = imgs.shape[1:]

    mosaic = ma.masked_all((nrows * imshape[0] + (nrows - 1) * border,
                            ncols * imshape[1] + (ncols - 1) * border),
                           dtype=np.float32)

    paddedh = imshape[0] + border
    paddedw = imshape[1] + border
    for i in xrange(nimgs):
        row = int(np.floor(i / ncols))
        col = i % ncols

        mosaic[row * paddedh:row * paddedh + imshape[0],
        col * paddedw:col * paddedw + imshape[1]] = imgs[i]
    return mosaic


def main():
    X = load_data()
    model = get_model()
    model.load_weights('/mnt/3E6227E362279F21/universiteit/natural computing/winter/models/reward_speed_run_3_episode_10315.h5')
    activations = get_activations(model, [X[0]], print_shape_only=False, layer_name='conv2')


    # pl.figure(figsize=(15, 15))
    # pl.title('conv1 weights')
    # nice_imshow(pl.gca(), make_mosaic(W, 6, 6), cmap=cm.binary)

    # Visualize convolution result (after activation)
    C1 = activations[0]
    C1 = np.squeeze(C1)
    print("C1 shape : ", C1.shape)

    pl.figure(figsize=(15, 15))
    pl.suptitle('convout2')
    nice_imshow(pl.gca(), make_mosaic(C1, 7, 7), cmap=cm.binary)


if __name__ == "__main__":
    main()