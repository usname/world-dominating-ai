## Todo
__Deep Q Learning implementations__
- Add Double DQN with Prioritised Replay
- [Add Agent with Recurrent Network](https://github.com/awjuliani/DeepRL-Agents/blob/master/Deep-Recurrent-Q-Network.ipynb)
- Add Frame Stacking to the Double DQN

__Code__
- Add multi GPU parallel training support / [single GPU multiple model support](https://github.com/fchollet/keras/issues/3287)
- Generalize code for multiple MS-DOS games:
    - Add dangerous dave
    - Settings per game
    - Main.py more general
- Add more mini-games from "The Games: Winter Challenge" (1991)
    - Add keys for each game
    - Add penalties for each game
- Add manual "agent"
- Add more detailed information logging (training time per episode, average reward, maximum speed etc.)
- Add more visualisation scripts
- Add support for frame-by-frame training 
- Add support for setting agent via settings
- Game downloading script
- OpenAI Gym integration
- Add tests
- Automatic menu detection
- Automatic key detection
- Utilize more of the screenshots made

## Useful links
- https://github.com/keon/deep-q-learning/blob/master/ddqn.py
- http://cs231n.stanford.edu/reports/2016/pdfs/112_Report.pdf
- http://cs231n.stanford.edu/reports/2016/pdfs/111_Report.pdf
- https://gist.github.com/awjuliani/35d2ab3409fc818011b6519f0f1629df
- http://rll.berkeley.edu/deeprlcourse/
- http://rll.berkeley.edu/deeprlcourse/docs/lec4.pdf
- https://github.com/jaara/AI-blog/blob/master/Seaquest-DDQN-PER.py
- https://jaromiru.com/2016/11/07/lets-make-a-dqn-double-learning-and-prioritized-experience-replay/