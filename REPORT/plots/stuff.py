import glob
import pandas as pd
import matplotlib.pyplot as plt
import seaborn


file_names = glob.glob('../FINAL/*.csv')
for file_name in file_names:
    print "File name: %s" % file_name
    df = pd.read_csv(file_name)
    finished = len(df.loc[df['done'] == 1.0])
    print "Finished: %d" % finished
    not_finished = len(df.loc[df['done'] != 1.0])
    print "Not finished: %d" % not_finished
    ratio = float(finished) / (float(not_finished) + float(finished))
    print "Ratio: %.3f" % ratio
    print ""

    plt.plot(df['episode_id'], map(lambda x: min(x, 80), df['action_none']))
    plt.plot(df['episode_id'], map(lambda x: min(x, 80), df['action_left']))
    plt.plot(df['episode_id'], map(lambda x: min(x, 80), df['action_right']))
    plt.plot(df['episode_id'], map(lambda x: min(x, 80), df['action_down']))

    plt.xlabel('Episode')
    plt.ylabel('Keypresses')
    #plt.title('Action counts per Episode')
    plt.legend(['No action', 'Left', 'Right', 'Down'])
    plt.grid(True)
    #plt.savefig("test.png")
    plt.show()
