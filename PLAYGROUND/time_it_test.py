import glob
import logging
import os
import random
import time
import numpy as np
import pyautogui
from PIL import Image
from collections import deque
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D
from keras.optimizers import Adam
from keras.preprocessing.image import img_to_array, load_img
from timeit import Timer

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("topdown")
logger.setLevel(logging.INFO)

class Settings:
    image_path = '/home/ivan/.dosbox/capture/'
    game_path = "/home/ivan/RU/WINTER/"

    episodes = 1
    train_interval_episodes = 1
    dosbox_icon_index = 6

    state_size = (160, 208, 3) # HxWxC

    actions = ['','Left','Right','Down']
    default_action = 0
    action_size = len(actions)

def reward_naive(game_states):
    updated_game_states = []
    final_game_state = game_states[-1]
    
    for game_state in game_states:
        game_state['reward'] = float(final_game_state['done'])
        updated_game_states.append(game_state)
        
    return updated_game_states

class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=2000)
        self.gamma = 0.95    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.001
        self.model = self._build_model()

    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3),
                         activation='relu',
                         input_shape=self.state_size))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))
        model.add(Flatten())
        model.add(Dense(24, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(24, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))     # TODO: different activation
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))    # TODO: different loss function
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        #if np.random.rand() <= self.epsilon:
        #    return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if done == 0:
              target = reward + self.gamma * \
                       np.amax(self.model.predict(next_state)[0])
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)

def check_time_speed(img):
	# The pattern matching
	# Don't look at this code, it will make you feel dirty
	width = 6
	height = 8

	color_corners = (121, 121, 121)
	color_sides = (16, 16, 16)

	corners = {
		# time
		'min_1': (263, 175),
		'sec_1': (274, 175),
		'sec_2': (282, 175),
		'msec_1': (293, 175),
		'msec_2': (301, 175),

		# speed
		'km_1': (263, 185),
		'km_2': (271, 185),
		'hm_1': (282, 185),
		'hm_2': (290, 185),
	}

	result = {}

	for value, coordinates in corners.items():
		color1 = img.getpixel((coordinates[0] + width, coordinates[1]))
		color2 = img.getpixel((coordinates[0], coordinates[1]))

		assert color1 == color_corners or color2 == color_corners or value == 'km_1', "Corners for time not found, %s" % (
		value)

		x, y = coordinates

		top = (x + (width / 2), y)
		middle = (x + (width / 2), y + (height / 2))
		bottom = (x + (width / 2), y + height)
		left_top = (x, y + (height / 4))
		left_bottom = (x, y + (height / 4 * 3))
		right_top = (x + width, y + (height / 4))
		right_bottom = (x + width, y + (height / 4 * 3))

		present = []
		if img.getpixel(top) == color_sides:
		    present.append('top')
		if img.getpixel(middle) == color_sides:
		    present.append('middle')
		if img.getpixel(bottom) == color_sides:
		    present.append('bottom')
		if img.getpixel(left_top) == color_sides:
		    present.append('left_top')
		if img.getpixel(left_bottom) == color_sides:
		    present.append('left_bottom')
		if img.getpixel(right_top) == color_sides:
		    present.append('right_top')
		if img.getpixel(right_bottom) == color_sides:
		    present.append('right_bottom')

		sides_0 = ['top', 'bottom', 'left_top', 'left_bottom', 'right_top', 'right_bottom']
		if present == sides_0 or present == []:
		    result[value] = '0'
		sides_1 = ['right_top', 'right_bottom']
		if present == sides_1:
		    result[value] = '1'
		sides_2 = ['top', 'middle', 'bottom', 'left_bottom', 'right_top']
		if present == sides_2:
		    result[value] = '2'
		sides_3 = ['top', 'middle', 'bottom', 'right_top', 'right_bottom']
		if present == sides_3:
		    result[value] = '3'
		sides_4 = ['middle', 'left_top', 'right_top', 'right_bottom']
		if present == sides_4:
		    result[value] = '4'
		sides_5 = ['top', 'middle', 'bottom', 'left_top', 'right_bottom']
		if present == sides_5:
		    result[value] = '5'
		sides_6 = ['top', 'middle', 'bottom', 'left_top', 'left_bottom', 'right_bottom']
		if present == sides_6:
		    result[value] = '6'
		sides_7 = ['top', 'right_top', 'right_bottom']
		if present == sides_7:
		    result[value] = '7'
		sides_8 = ['top', 'middle', 'bottom', 'left_top', 'left_bottom', 'right_top', 'right_bottom']
		if present == sides_8:
		    result[value] = '8'
		sides_9 = ['top', 'middle', 'left_top', 'right_top', 'right_bottom']
		if present == sides_9:
		    result[value] = '9'
	# Warned you...
	time = int(result['min_1']) * 60 * 100 + int(result['sec_1']) * 10 * 100 + int(result['sec_2']) * 100 + int(result['msec_1']) * 10 + int(result['msec_2'])
	if time == 0:
		time = 1
	speed = int(result['km_1']) * 10 * 100 + int(result['km_2']) * 100 + int(result['hm_1']) * 10 + int(result['hm_2'])
	return time, speed

def check_stats(rgb_img):
    # Check menu pixel
    menu_pixel = (76, 31)
    r, g, b = rgb_img.getpixel(menu_pixel)
    menu = (r == 251 and g == 40 and b == 40)

    # Chaeck finish pixel GAME 3
    finish_pixel = (21, 138)
    r, g, b = rgb_img.getpixel(finish_pixel)
    finish = (r != 56 or g != 186 or b != 186)

    if finish and menu:
        done = 1
    elif not finish and menu:
        done = -1
    else:
        done = 0

    time, speed = check_time_speed(rgb_img)
    return done, time, speed

def play(agent):
	logger.info("Play!")
	game_states = []

	current_img = Image.open('default.png')
	current_img = current_img.convert('RGB')
	current_frame = current_img.crop((112, 1, 112 + Settings.state_size[1], 1 + Settings.state_size[0]))
	current_frame = img_to_array(current_frame)
	current_frame = current_frame.reshape((1,) + current_frame.shape)

	done, time, speed = check_stats(current_img)
	state = (current_frame, speed, time)

	action = agent.act(current_frame)
	logger.info("Action %s" % Settings.actions[action])

	game_state = {'state': state,
					'action': action,
					'reward': None,
					'next_state': None,
					'done': done}
	game_states.append(game_state)

	logger.info("done: %d, time: %d (ms), speed: %d (dam/s)" % (done, state[2], state[1]))

	previous_action = action
	next_state = state
	next_done = done

agent = DQNAgent(Settings.state_size, Settings.action_size)
t = Timer(lambda: play(agent))
print(t.timeit(number=1))
