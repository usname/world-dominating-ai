import numpy as np
import pyscreenshot as ImageGrab
import pyautogui
from PIL import Image
import cv2
import os
from cnn import CNN
#from dqn import DQN
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class API(object):
    def __init__(self, game):
        # Size of the frame to capture
        self.frame_size = (704, 450)

        # Offset x where the frame is
        self.offset_x = 64

        # Offset y where the frame is
        self.offset_y = 50

        # The game to play
        self.game = game

    def start_dosbox(self):
        logger.info("Click dosbox icon")
        pyautogui.click(33, 466, pause=3.0)

        pyautogui.typewrite('mount C: "%s"' % self.game.path, pause=2.0)
        pyautogui.press("enter", pause=1.5)

        pyautogui.typewrite("C:", pause=1.5)
        pyautogui.press("enter", pause=1.5)

        pyautogui.typewrite("%s" % self.game.executable, pause=1.5)
        pyautogui.press("enter", pause=1.5)

    def stop_dosbox(self):
        logger.info("Click close dosbox")
        pyautogui.click(81, 39, pause=1.0)

    def screen_record(self, iters=5000):
        # fourcc = cv2.VideoWriter_fourcc(*'XVID')
        # out = cv2.VideoWriter('debug.avi', fourcc, fps=24, frameSize=(self.frame_size[0] - self.offset_x, self.frame_size[1] - self.offset_y))

        for n in range(iters):
            img = ImageGrab.grab(bbox=(self.offset_x, self.offset_y, self.frame_size[0], self.frame_size[1]))
            f = np.array(img)

            # if f is None:
            #    break

            f = cv2.cvtColor(f, cv2.COLOR_BGR2RGB)

            f2 = f.copy()
            yield f2
            # if debug:
            #    out.write(f2)

        # if debug:
        #    out.release()
        cv2.destroyAllWindows()

    def collect_data(self, directory):
        logger.info("Starting dosbox")
        self.start_dosbox()

        logger.info("Starting game")
        self.game.start()

        logger.info("Start screen recording")
        for nr, img in enumerate(self.screen_record()):
            cv2.imwrite(os.path.join(directory, "frame-" + str(nr) + ".png"), img)

        logger.info("Ending game")
        self.game.end()

        logger.info("Close dosbox")
        self.stop_dosbox()

    def play(self):
        logger.info("Starting dosbox")
        self.start_dosbox()

        logger.info("Starting game")
        self.game.start()

        cnn = CNN()
        #dqn = DQN()

        logger.info("Start screen recording")
        for img in self.screen_record():
            img = Image.fromarray(img)
            response = cnn.process_frame(img)

            if not response['alive']:
                self.game.restart()
            else:
                suggested_action = 'right' #dqn.process(response, img)
                self.game.action(suggested_action)

        logger.info("Ending game")
        self.game.end()

        logger.info("Close dosbox")
        self.stop_dosbox()