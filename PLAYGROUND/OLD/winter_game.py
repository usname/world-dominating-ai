from game import Game
import pyautogui
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class WinterGame(Game):
    def __init__(self):
        actions = {
            'accelerate': 'up',
            'brake': 'down',
            'left': 'left',
            'right': 'right'
        }

        super(WinterGame, self).__init__(path="/mnt/3E6227E362279F21/universiteit/natural computing/winter/bin/winter/",executable="WINTER.exe", actions=actions)

    def action(self, action):
        logger.info("Executing action '%s'" % action)
        pyautogui.press(action, pause=0.25)

    def _skip_intro(self):
        logger.info("Skipping intro")
        pyautogui.press('enter', pause=5.0)
        pyautogui.press('enter', pause=5.0)

    def _setup(self):
        logging.info("Going trough setup")
        pyautogui.press('right', pause=1.5)
        pyautogui.press('right', pause=1.5)
        pyautogui.press('enter', pause=1.5)
        pyautogui.press('enter', pause=1.5)

    def _training(self, sport='team_bobsled'):
        sports = {
            'team_bobsled': 3,
        }

        logger.info("Open training")
        pyautogui.press('enter', pause=1.5)

        logger.info("Focus correct sport")
        for _ in xrange(sports[sport]):
            pyautogui.press('right', pause=1.5)

        logger.info("Select correct sport")
        pyautogui.press('enter', pause=2.5)

        logger.info("Start go screen")
        pyautogui.press('enter', pause=2.5)

        logger.info("Start the sport round")
        if sport == 'team_bobsled':
            pyautogui.press('enter', pause=2.5)
        else:
            pyautogui.press('up', pause=2.5)

    def start(self):
        self._skip_intro()
        self._setup()
        self._training()

    def end(self):
        logger.info("End game")
        pyautogui.press('esc', pause=2.0)
        pyautogui.press('esc', pause=2.0)

    def restart(self):
        pass