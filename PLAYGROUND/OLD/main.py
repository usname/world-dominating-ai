from api import API
from winter_game import WinterGame
from wold3d_game import Wolf3dGame


def main():
    wolf = Wolf3dGame()
    winter = WinterGame()

    api = API(winter)
    #api.collect_data("data/winter/images/")
    api.play()


if __name__ == "__main__":
    main()