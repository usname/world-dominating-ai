from game import Game
import pyautogui
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Wolf3dGame(Game):
    def __init__(self):
        actions = {
            'forward': 'up',
            'backward': 'down',
            'left': 'left',
            'right': 'right',
            'shoot': 'ctrl',
            'open': 'space'
            # Alternatief met muis
        }

        super(Wolf3dGame, self).__init__(path="/mnt/3E6227E362279F21/universiteit/natural computing/winter/bin/wolf3d/", executable="WOLF3D.exe", actions=actions)

    def action(self, action):
        logger.info("Executing action '%s'" % action)
        pyautogui.press(action, pause=0.25)

    def _setup(self):
        pyautogui.press('right', pause=1.5)
        pyautogui.press('right', pause=1.5)
        pyautogui.press('enter', pause=1.5)
        pyautogui.press('enter', pause=1.5)

    def start(self):
        # [SETUP]
        logger.info("Click dosbox icon")
        pyautogui.click(33, 466, pause=1.5)
        # [/SETUP]

    def end(self):
        logger.info("End game")
        pyautogui.press('esc', pause=2.0)
        pyautogui.press('esc', pause=2.0)

    def restart(self):
        pass