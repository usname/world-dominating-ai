import keras
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D, Input
from keras import backend as K
from PIL import Image
import pandas as pd
import numpy as np
import logging
import os

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class CNN(object):
    def __init__(self):
        self.model = None
        if os.path.exists('winter_speed.h5'):
            self.load_model()
        else:
            self.train_model()

    def _build_model(self, num_classes, input_shape):
        input = Input(shape=input_shape)
        x = Conv2D(32, kernel_size=(3, 3),
                   activation='relu')(input)
        x = Conv2D(64, (3, 3), activation='relu')(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
        x = Dropout(0.25)(x)
        x = Flatten()(x)
        x = Dense(128, activation='relu')(x)
        x = Dropout(0.5)(x)
        x = Dense(128, activation='relu')(x)
        x = Dropout(0.5)(x)
        speed_1 = Dense(num_classes, name='speed_1', activation='softmax')(x)
        speed_2 = Dense(num_classes, name='speed_2', activation='softmax')(x)
        speed_3 = Dense(num_classes, name='speed_3', activation='softmax')(x)
        speed_4 = Dense(num_classes, name='speed_4', activation='softmax')(x)

        self.model = Model([input], [speed_1, speed_2, speed_3, speed_4])

    def train_model(self):
        batch_size = 64
        epochs = 300
        num_classes = 10

        split = 3100

        # input image dimensions
        img_rows, img_cols = 90, 21

        # the data, shuffled and split between train and test sets
        data_frame = pd.read_csv('./data/winter/labels/labels.csv')
        x = []
        y = [data_frame.speed_1.values, data_frame.speed_2.values, data_frame.speed_3.values, data_frame.speed_4.values]
        x_id = data_frame.id.values
        for id in x_id:
            img = Image.open("./data/winter/images/" + id + ".png")
            img = img.crop((506, 370, 596, 391))
            img.save("test_crop.png", "PNG")

            img = np.asarray(img)
            x.append(img)

        (x_train, y_train), (x_test, y_test) = (x[:split], [ys[:split] for ys in y]), (x[split:], [ys[split:] for ys in y])
        x_train = np.array(x_train)
        x_test = np.array(x_test)

        if K.image_data_format() == 'channels_first':
            x_train = x_train.reshape(x_train.shape[0], 3, img_rows, img_cols)
            x_test = x_test.reshape(x_test.shape[0], 3, img_rows, img_cols)
            input_shape = (3, img_rows, img_cols)
        else:
            x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 3)
            x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 3)
            input_shape = (img_rows, img_cols, 3)

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255
        print('x_train shape:', x_train.shape)
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')

        # convert class vectors to binary class matrices
        y_train = [keras.utils.to_categorical(y_trains, num_classes) for y_trains in y_train]
        y_test = [keras.utils.to_categorical(y_tests, num_classes) for y_tests in y_test]

        self._build_model(num_classes, input_shape)
        self.model.compile(loss=keras.losses.categorical_crossentropy,
                           optimizer=keras.optimizers.Adam(lr=0.001),
                           metrics=['accuracy'])

        logger.info("Start fitting process")
        self.model.fit(x_train, y_train,
                       batch_size=batch_size,
                       epochs=epochs,
                       verbose=2,
                       validation_data=(x_test, y_test))
        self.model.save_weights('winter_speed.h5')
        score = self.model.evaluate(x_test, y_test, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])

    def load_model(self):
        num_classes = 10

        # input image dimensions
        img_rows, img_cols = 90, 21
        if K.image_data_format() == 'channels_first':
            input_shape = (3, img_rows, img_cols)
        else:
            input_shape = (img_rows, img_cols, 3)

        self._build_model(num_classes, input_shape)
        self.model.load_weights('winter_speed.h5')

    def process_frame(self, img):
        img = img.crop((506, 370, 596, 391))
        img = np.asarray(img)
        x_test = np.array([img])
        x_test = x_test.reshape(x_test.shape[0], 90, 21, 3)
        return self.process(x_test)

    def process(self, img):
        predicted_speed = self.model.predict(img)
        speed_1 = np.argmax(predicted_speed[0])
        speed_2 = np.argmax(predicted_speed[1])
        speed_3 = np.argmax(predicted_speed[2])
        speed_4 = np.argmax(predicted_speed[3])
        speed = "%d%d.%d%d" % (speed_1, speed_2, speed_3, speed_4)
        speed = float(speed)

        response = {
            'alive': True,
            'speed': speed,
            'time': 10.0,
            'user_location': (10.0, 15.0)
        }

        return response
