import csv
import glob
import logging
import os
import random
import time
from collections import deque

import numpy as np
import pyautogui
import sys
from PIL import Image
from keras import backend as K
from keras.layers import Dense, Conv2D, Flatten, Input, concatenate
from keras.models import Model
from keras.optimizers import Adam
from keras.preprocessing.image import img_to_array
from xdo import Xdo

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("topdown")
logger.setLevel(logging.INFO)


# CHANGE THESE SETTINGS
# ALSO MAKE SURE DOSBOX IS THE 7TH ICON IN YOUR UBUNTU LEFT SIDEBAR
# THE WINDOW SHOULD OPEN IN THE TOP-LEFT CORNER, OTHERWISE DRAG IT THERE
class Settings:
    image_path = '/home/mrplank/.dosbox/capture/'
    game_path = "/home/mrplank/winter/"

    experiment_runs = 1             # Do multiple runs with different random seeds
    episodes = 1000                   # Maximum number of episodes
    timeout = 150                   # Timout in seconds
    batch_size = 32
    dosbox_icon_index = 7

    state_size = (160, 208, 3)  # HxWxC


    actions = ['', 'Left', 'Right', 'Down']
    default_action = 0
    action_size = len(actions)


# TODO: TRY DIFFERENT REWARD FUNCTIONS
# TODO: SAVE MEMORY
# TODO: POST-PROCESS EPISODE (REWARD + REMOVE LAST FRAMES)
# TODO: IMPROVE MODEL
# TODO: REPORT ON CONVERGENCE TIME ETC. FOR REWARD FUNCTIONS
# TODO: DEBUGGING
# TODO: OPTIONAL. IMPROVE SPEED
# TODO: OPTIONAL. OPTIMIZE CODE
# TODO: OPTIONAL. TIMEOUT
# TODO: OPTIONAL. CHOOSE LOSS FUNCTION CNN?


# GOAL:
# - PRIMAIRY: FINISH
# - SECONDARY: MINIMIZE TIME
# def reward_naive(done, time, distance, avg_speed):
#     return done
#
#
# def reward_time_scaled(done, time, distance, avg_speed):
#     if done > 0:
#         return (float(done) * 100.0) / (float(time) - 50.0 * 100.0)
#     else:
#         return (float(done) * 100.0) / float(time)


def reward_distance(done, time, distance, avg_speed):
    return distance


def reward_distance_avg_speed(done, time, distance, avg_speed):
    return distance * avg_speed


def reduce_reward(x):
    x['reward'] = x['reward'] / 10.0
    return x


def reward_wrapper(game_states, strategy=reward_distance_avg_speed):
    updated_game_states = []
    final_game_state = game_states[-1]

    for game_state in game_states:
        game_state['reward'] = float(strategy(final_game_state['done'], final_game_state['state'][2], final_game_state['state'][3], mean(final_game_state['state'][4])))
        updated_game_states.append(game_state)

    if final_game_state['done'] < 1:
        updated_game_states[-5:] = map(reduce_reward, updated_game_states[-5:])

    return updated_game_states


def mean(l):
    lz = list(l)
    return np.array(lz).mean()

class Dosbox:
    def __init__(self):
        pass

    def start(self, game):
        logger.info("Click dosbox icon")
        #pyautogui.hotkey('win','6', pau6se=5.0)
        pyautogui.click(33, 466, pause=3.0)
        # sys.exit()

        pyautogui.typewrite('mount C: "%s"' % Settings.game_path,
                            pause=0.5)
        pyautogui.press("enter", pause=0.5)

        pyautogui.typewrite("C:", pause=0.5)
        pyautogui.press("enter", pause=0.5)

        pyautogui.typewrite("%s" % "WINTER.EXE", pause=0.5)
        pyautogui.press("enter", pause=2.0)
        #sys.exit()


    def pause(self):
        logger.info("Pause game")
        pyautogui.hotkey('pause')

    def stop(self):
        logger.info("Stop dosbox")
        pyautogui.hotkey('ctrl', 'f9', interval=0.2, pause=1.0)

    def screenshot(self):
        logger.info("Take screenshot")
        pyautogui.hotkey('ctrl', 'f5', interval=0.2, pause=1.0)
        return max(glob.iglob(Settings.image_path + '*.png'), key=os.path.getctime)


class Winter:
    def __init__(self):
        pass

    def menu_fix(self):
        self._skip_intro()
        self._setup()
        self._training()

    def _skip_intro(self):
        logger.info("Skipping intro")
        pyautogui.press('enter', pause=2.0)
        pyautogui.press('enter', pause=2.0)

    def _setup(self):
        logger.info("Going trough setup")
        pyautogui.press('right', pause=0.5)
        pyautogui.press('right', pause=0.5)
        pyautogui.press('enter', pause=1.5)
        pyautogui.press('enter', pause=1.5)

    def _training(self):
        logger.info("Open training")
        pyautogui.press('enter', pause=1.5)

        logger.info("Focus correct sport")
        for _ in xrange(3):
            pyautogui.press('right', pause=0.5)

        logger.info("Select correct sport")
        pyautogui.press('enter', pause=2.5)

        logger.info("Start go screen")
        pyautogui.press('enter', pause=2.5)

        logger.info("Start the sport round")

        logger.info("Focus on game")
        pyautogui.click(100, 300, pause=3.0)


# Shamelessly taken from https://github.com/keon/deep-q-learning/blob/master/dqn.py
class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=2000)
        self.gamma = 0.95  # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.001
        self.model = self._build_model()
        self.target_model = self._build_model()
        self.update_target_model()

    def _huber_loss(self, target, prediction):
        # sqrt(1+error^2)-1
        error = prediction - target

        return K.mean(K.sqrt(1 + K.square(error)) - 1, axis=-1)

    def _build_model(self, input_shape=(84, 84, 1)):
        frame_input = Input(shape=input_shape, name='frame_input')
        speed_input = Input(shape=(1,), name='speed_input')

        x = Conv2D(16, kernel_size=(8, 8), strides=(2, 2),
                   activation='relu')(frame_input)
        x = Conv2D(32, kernel_size=(4, 4), strides=(2, 2),
                   activation='relu')(x)

        x = Flatten()(x)
        x = concatenate([x, speed_input])

        x = Dense(256, activation='relu')(x)
        output = Dense(self.action_size, activation='softmax')(x)

        model = Model(inputs=[frame_input, speed_input], outputs=[output])
        model.compile(loss=self._huber_loss, optimizer=Adam(lr=self.learning_rate))
        return model

    def update_target_model(self):
        # copy weights from model to target_model
        self.target_model.set_weights(self.model.get_weights())

    def preprocess_image(self, rgb_image):
        gray_image = rgb_image.convert('L')
        frame = gray_image.crop((112, 1, 112 + self.state_size[1], 1 + self.state_size[0]))
        frame = frame.resize((110, 84))
        frame = frame.crop((13, 0, 97, 84))
        # frame.save('test_image.png')
        frame_array = img_to_array(frame)
        return frame_array.reshape((1,) + frame_array.shape)

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = self.model.predict(state)
            if done:
                target[0][action] = reward
            else:
                a = self.model.predict(next_state)[0]
                t = self.target_model.predict(next_state)[0]
                target[0][action] = reward + self.gamma * t[np.argmax(a)]
            self.model.fit(state, target, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)


class Env:
    def action(self, name='', previous=None):
        logger.info("Action '%s'" % str(name))
        if name != 'Return':
            if previous is not None and previous != name and previous != '':
                logger.info("Previous '%s'" % str(previous))
                self.xdo.send_keysequence_window_up(self.win_id, previous)
            if name != '':
                self.xdo.send_keysequence_window_down(self.win_id, name)

            # self.xdo.send_keysequence_window_down(self.win_id, 'Alt_L')
            # self.xdo.send_keysequence_window_down(self.win_id, 'F12')
            # time.sleep(0.01)
            # self.xdo.send_keysequence_window_up(self.win_id, 'Alt_L')
            # self.xdo.send_keysequence_window_up(self.win_id, 'F12')

        else:
            self.xdo.send_keysequence_window(self.win_id, name)
            pyautogui.press(name.lower(), presses=100, interval=0.01, pause=1.0)

    def __init__(self):
        self.dosbox = Dosbox()
        self.winter = Winter()
        self.xdo = Xdo()

    def setup(self):
        self.dosbox.start('WINTER.EXE')
        self.winter.menu_fix()

        self.win_id = self.xdo.get_active_window()
        logger.info(self.xdo.get_window_name(self.win_id))

        #self.reset()

    def check_time_speed(self, img):
        # The pattern matching
        # Don't look at this code, it will make you feel dirty
        width = 6
        height = 8

        color_corners = (121, 121, 121)
        color_sides = (16, 16, 16)

        corners = {
            # time
            'min_1': (263, 175),
            'sec_1': (274, 175),
            'sec_2': (282, 175),
            'msec_1': (293, 175),
            'msec_2': (301, 175),

            # speed
            'km_1': (263, 185),
            'km_2': (271, 185),
            'hm_1': (282, 185),
            'hm_2': (290, 185),
        }

        result = {}

        for value, coordinates in corners.iteritems():
            color1 = img.getpixel((coordinates[0] + width, coordinates[1]))
            color2 = img.getpixel((coordinates[0], coordinates[1]))

            assert color1 == color_corners or color2 == color_corners or value == 'km_1', "Corners for time not found, %s" % (
                value)

            x, y = coordinates

            top = (x + (width / 2), y)
            middle = (x + (width / 2), y + (height / 2))
            bottom = (x + (width / 2), y + height)
            left_top = (x, y + (height / 4))
            left_bottom = (x, y + (height / 4 * 3))
            right_top = (x + width, y + (height / 4))
            right_bottom = (x + width, y + (height / 4 * 3))

            present = []
            if img.getpixel(top) == color_sides:
                present.append('top')
            if img.getpixel(middle) == color_sides:
                present.append('middle')
            if img.getpixel(bottom) == color_sides:
                present.append('bottom')
            if img.getpixel(left_top) == color_sides:
                present.append('left_top')
            if img.getpixel(left_bottom) == color_sides:
                present.append('left_bottom')
            if img.getpixel(right_top) == color_sides:
                present.append('right_top')
            if img.getpixel(right_bottom) == color_sides:
                present.append('right_bottom')

            sides_0 = ['top', 'bottom', 'left_top', 'left_bottom', 'right_top', 'right_bottom']
            if present == sides_0 or present == []:
                result[value] = '0'
            sides_1 = ['right_top', 'right_bottom']
            if present == sides_1:
                result[value] = '1'
            sides_2 = ['top', 'middle', 'bottom', 'left_bottom', 'right_top']
            if present == sides_2:
                result[value] = '2'
            sides_3 = ['top', 'middle', 'bottom', 'right_top', 'right_bottom']
            if present == sides_3:
                result[value] = '3'
            sides_4 = ['middle', 'left_top', 'right_top', 'right_bottom']
            if present == sides_4:
                result[value] = '4'
            sides_5 = ['top', 'middle', 'bottom', 'left_top', 'right_bottom']
            if present == sides_5:
                result[value] = '5'
            sides_6 = ['top', 'middle', 'bottom', 'left_top', 'left_bottom', 'right_bottom']
            if present == sides_6:
                result[value] = '6'
            sides_7 = ['top', 'right_top', 'right_bottom']
            if present == sides_7:
                result[value] = '7'
            sides_8 = ['top', 'middle', 'bottom', 'left_top', 'left_bottom', 'right_top', 'right_bottom']
            if present == sides_8:
                result[value] = '8'
            sides_9 = ['top', 'middle', 'left_top', 'right_top', 'right_bottom']
            if present == sides_9:
                result[value] = '9'
            # Smerige hack als plaatje kut is
            if not value in result:
                result[value] = '0'

        # Warned you...
        time = int(result['min_1']) * 60 * 100 + int(result['sec_1']) * 10 * 100 + int(result['sec_2']) * 100 + int(
            result['msec_1']) * 10 + int(result['msec_2'])
        if time == 0:
            time = 1
        speed = int(result['km_1']) * 10 * 100 + int(result['km_2']) * 100 + int(result['hm_1']) * 10 + int(
            result['hm_2'])
        return time, speed

    def check_stats(self, rgb_img):
        # Check menu pixel
        menu_pixel = (76, 31)
        r, g, b = rgb_img.getpixel(menu_pixel)
        menu = (r == 251 and g == 40 and b == 40)

        # Check finish pixel GAME 3
        finish_pixel = (21, 138)
        r, g, b = rgb_img.getpixel(finish_pixel)
        finish = (r != 56 or g != 186 or b != 186)

        if finish and menu:
            done = 1
        elif not finish and menu:
            done = -1
        else:
            done = 0

        time, speed = self.check_time_speed(rgb_img)
        return done, time, speed

    def check_distance(self, distance, time, speed, next_time, next_speed):
        return distance + (((next_time - time) / 10.0) * ((next_speed + speed) / (2.0 * 3600.0)))

    def play(self, agent):
        logger.info("Play!")

        game_states = []
        previous_action = Settings.default_action

        current_img = Image.open('default.png')
        current_img = current_img.convert('RGB')
        current_frame = agent.preprocess_image(current_img)

        done, timez, speed = self.check_stats(current_img)
        distance = 0
        speeds_list = [0.0]
        state = [current_frame, np.array([speed]), timez, distance, speeds_list]

        #for time in range(1, Settings.timeout):
        while True:
            action = agent.act(state[:2])

            if done != 0:
                agent.update_target_model()
                game_state = {'state': state,
                              'action': action,
                              'reward': None,
                              'next_state': None,
                              'done': done}
                game_states.append(game_state)

                logger.info("done: %d, time: %d (10 x ms), speed: %d (dam/h), distance: %d (m), avg speed: %f" % (done, state[2], state[1], state[3], mean(state[4])))
                break

            next_img_file = self.step(Settings.actions[action], previous=Settings.actions[previous_action])
            logger.info("Image: %s" % next_img_file)
            next_img = Image.open(next_img_file)
            next_img = next_img.convert('RGB')
            next_frame = agent.preprocess_image(next_img)

            next_done, next_time, next_speed = self.check_stats(next_img)
            next_distance = self.check_distance(state[3], state[2], state[1], next_time, next_speed)

            speeds_list.append(next_speed)
            next_state = [next_frame, np.array([next_speed]), next_time, next_distance, speeds_list]

            game_state = {'state': state,
                          'action': action,
                          'reward': None,
                          'next_state': next_state,
                          'done': done}
            game_states.append(game_state)

            logger.info("done: %d, time: %d (10 x ms), speed: %d (dam/h), distance: %d (m), avg speed: %f.04" % (done, state[2], state[1], state[3], mean(state[4])))

            previous_action = action
            state = next_state
            done = next_done

        game_states = reward_wrapper(game_states)
        for game_state in game_states:
            if game_state['next_state'] is not None:
                next_state = game_state['next_state'][:2]
            else:
                next_state = None
            agent.remember(game_state['state'][:2],
                           game_state['action'],
                           game_state['reward'],
                           next_state,
                           game_state['done'])

        self.delete_images()

        return game_states[-1]

    def delete_images(self):
        img_files = [f for f in os.listdir(Settings.image_path) if f.endswith('.png')]
        for img_file in img_files:
            os.remove(os.path.join(Settings.image_path, img_file))

    def teardown(self):
        self.dosbox.pause()
        self.dosbox.stop()

    def step(self, action, previous=None):
        self.action(action, previous)

        return self.dosbox.screenshot()

    def reset(self):
        # Make Sure that in menu the right unit is selected
        img_file = self.dosbox.screenshot()
        im = Image.open(img_file)
        im_rgb = im.convert('RGB')
        while im_rgb.getpixel((200, 50)) != (85, 85, 255):
            pyautogui.press('down')
            img_file = self.dosbox.screenshot()
            im = Image.open(img_file)
            im_rgb = im.convert('RGB')

        self.action('Return')
        self.action('Return')


if __name__ == "__main__":
    env = Env()
    env.setup()

    for run_id in xrange(1, Settings.experiment_runs + 1):
        random.seed(2017 + run_id)
        np.random.seed(2017 + run_id)

        agent = DQNAgent(Settings.state_size, Settings.action_size)

        ####
        # WARMUP
        ####
        logger.info("Warm up model")
        current_img = Image.open('default.png')
        current_img = current_img.convert('RGB')
        current_frame = agent.preprocess_image(current_img)

        done, timez, speed = env.check_stats(current_img)
        distance = 0
        speeds = [0.0]
        state = [current_frame, np.array([speed]), timez, distance]

        agent.act(state)
        #####

        with open('episodes_%d.csv' % run_id, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(['Episode', 'Reward', 'Time', 'Distance'])

        results = {}

        env.reset()

        for episode_id in xrange(1, Settings.episodes + 1):
            logger.info("Playing game %d of %d" % (episode_id, Settings.episodes))
            final_game_state = env.play(agent)
            results['episode_' + str(episode_id)] = (final_game_state['reward'], final_game_state['state'][2], final_game_state['state'][3])

            agent.replay(Settings.batch_size)
            agent.save('models/Model_%d_Episodes.h5' % episode_id)

            for key in sorted(results.iterkeys()):
                logger.info(
                    'Reward for \'%s\' is %.9f. Time for episode was %d ms. Distance was %d m.' % (key, results[key][0], results[key][1], results[key][2]))
            fields = ['episode_' + str(episode_id), results['episode_' + str(episode_id)][0], results['episode_' + str(episode_id)][1], results['episode_' + str(episode_id)][2]]
            with open('episodes_%d.csv' % run_id, 'a') as f:
                writer = csv.writer(f)
                writer.writerow(fields)
            if episode_id < Settings.episodes:
                env.reset()
    env.teardown()
