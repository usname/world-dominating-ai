import abc


class Game(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, path, executable, actions):
        self.path = path
        self.executable = executable
        self.actions = actions

    @abc.abstractmethod
    def start(self):
        raise NotImplementedError('users must define start to use the game class')

    @abc.abstractmethod
    def end(self):
        raise NotImplementedError('users must define start to use the game class')

    @abc.abstractmethod
    def restart(self):
        raise NotImplementedError('users must define start to use the game class')