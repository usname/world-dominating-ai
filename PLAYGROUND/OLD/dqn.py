import random
from collections import deque
from keras.layers import Dense, Dropout, Flatten, concatenate
from keras.layers import Conv2D, MaxPooling2D, Input
from keras.models import Model
from keras.optimizers import Adam
import numpy as np
from PIL import Image


class DQNAgent(object):
    def __init__(self, target_size=(84, 84), queue_size=2000, gamma=0.95, epsilon=1.0,
                 epsilon_min=0.01, epsilon_decay=0.995, learning_rate=0.001):
        self.memory = deque(maxlen=queue_size)
        
        self.gamma = gamma
        self.epsilon = epsilon
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_decay

        self.target_size = target_size
        self.actions = {0: 'nothing', 1: 'brake', 2: 'left', 3: 'right'}

        self.model = self._build_model(len(self.actions), self.target_size + (3,))
        self.model.compile(optimizer=Adam(lr=learning_rate), loss='mse')

    def _preprocess_frame(self, img):
        img = img.crop((225, 0, 640, 324))
        img = img.resize((self.target_size[1], self.target_size[0]))
        img = np.asarray(img)
        x_test = np.array([img])
        return x_test
        
    def _build_model(self, num_classes, input_shape):
        frame_input = Input(shape=input_shape, name='frame_input')
        speed_input = Input(shape=(1,), name='speed_input')
        
        x = Conv2D(32, kernel_size=(7, 7), strides=(2, 2), activation='relu')(frame_input)
        x = Conv2D(64, kernel_size=(5, 5), strides=(2, 2), activation='relu')(x)
        x = Conv2D(64, kernel_size=(5, 5), activation='relu')(x)
        
        x = Flatten()(x)
        x = concatenate([x, speed_input])
        
        x = Dense(256, activation='relu')(x)
        x = Dropout(0.5)(x)
        output = Dense(num_classes, activation='softmax')(x)
        
        return Model(inputs=[frame_input, speed_input], outputs=[output])

    def remember(self, state, action, reward, next_state, done):
        frame, speed = state
        frame = self._preprocess_frame(frame)
        
        next_frame, next_speed = next_state
        next_frame = self._preprocess_frame(next_frame)
        
        self.memory.append(((frame, speed), action, reward, (next_frame, next_speed), done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(len(self.actions))
            
            
        action_values = self.model.predict(state)
        return self.actions[np.argmax(action_values[0])]
        
    def replay(self, batch_size):
        # Rename + refactor to build a batch and fit on batch + state representation
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
              target = reward + self.gamma * \
                       np.max(self.model.predict(next_state)[0])
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
